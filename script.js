$( document ).ready(function(){

	var k = false;

	$('.header__container-menu--button, .menu__container--button-close').click(function(){/*При нажатии на кпонки закрытия и открытия*/
		var backgroundOnclick = $('.all__container');
		var allBody = $('body');
		$( ".menu" ).animate({width:'toggle'},350);
		if(k==false){
			backgroundOnclick.addClass('header__container-menu--onclick');
			allBody.addClass('onclick-scroll--RIP');
			k=true;
		}
		else if (k==true){
			backgroundOnclick.removeClass('header__container-menu--onclick');
			allBody.removeClass('onclick-scroll--RIP');
			k=false;
		}
	});
	$(document).keydown(function(eventObject){/*При нажатии на ESC*/
		var backgroundOnclick = $('.all__container');
		var allBody = $('body');
		if( eventObject.which == 27 ){
			if (k==true){
				$( ".menu" ).animate({width:'hide'},350);
				backgroundOnclick.removeClass('header__container-menu--onclick');
				allBody.removeClass('onclick-scroll--RIP');
				k=false;
			}
		};
	});
	$(document).mouseup(function(e){/*При нажатии на "пустое пространство"*/
		var div = $('.menu');
		var backgroundOnclick = $('.all__container');
		var allBody = $('body');
		if (!div.is(e.target)
		    && div.has(e.target).length === 0) {
			div.animate({width:'hide'},350);
				backgroundOnclick.removeClass('header__container-menu--onclick');
				allBody.removeClass('onclick-scroll--RIP');
				k=false;
		}
	});


//ПРИ-НАВЕДЕНИИ-НА-ОДНУ-ИЗ-КАРТ-НАПРАВЛЕНИЯ-ВЫСВЕЧИВАЕТСЯ-КНОПКА-ПОДРОБНЕЕ//
	$('.main__direction--first').hover(function() {
    	$('.main__direction--first > a').show();
		}, 
	function() {
    	$('.main__direction--first > a').hide();
	});
	$('.main__direction--second').hover(function() {
    	$('.main__direction--second > a').show();
		}, 
	function() {
    	$('.main__direction--second > a').hide();
	});
	$('.main__direction--dopFirst').hover(function() {
    	$('.main__direction--dopFirst > a').show();
		}, 
	function() {
    	$('.main__direction--dopFirst > a').hide();
	});
	$('.main__direction--dopSecond').hover(function() {
    	$('.main__direction--dopSecond > a').show();
		}, 
	function() {
    	$('.main__direction--dopSecond > a').hide();
	});

	$('.main__direction--third').hover(function() {
    	$('.main__direction--third > a').show();
		}, 
	function() {
    	$('.main__direction--third > a').hide();
	});

	$('.main__direction--four').hover(function() {
    	$('.main__direction--four > a').show();
		}, 
	function() {
    	$('.main__direction--four > a').hide();
	});
/////////////////////////////////////////////////////////////////////////////
        var swiper = new Swiper('.main__news--slider', {
	      slidesPerView: 'auto',
	      spaceBetween: 30,
	      slidesPerGroup: 1,
	      loop: true,
	      loopFillGroupWithBlank: true,
	      navigation: {
	        nextEl: '.main__news-swiper--next',
	        prevEl: '.main__news-swiper--prev',
	      },
	      breakpoints: {
	      	1200:{
	      		spaceBetween: 30
	      	},
	      	767:{
	      		spaceBetween: 20
	      	},
            320: {
                spaceBetween: 10
            },
          }
		});
		var swiper1 = new Swiper('.main__russian-place--slider', {
	      slidesPerView: 'auto',
	      spaceBetween: 30,
	      slidesPerGroup: 1,
	      loop: true,
	      loopFillGroupWithBlank: true,
	      navigation: {
	        nextEl: '.main__russian-place--next',
	        prevEl: '.main__russian-place--prev',
	      },
	      	breakpoints: {
	      	1200:{
	      		spaceBetween: 30
	      	},
	      	767:{
	      		spaceBetween: 20
	      	},
            320: {
                spaceBetween: 10
            },
          }
		});
		var swiper2 = new Swiper('.main__hot-offer--slider', {
		  slidesPerView: 'auto',
	      spaceBetween: 30,
	      slidesPerGroup: 1,
	      loop: true,
	      loopFillGroupWithBlank: true,
	      navigation: {
	        nextEl: '.main__hot-offer-swiper--next',
	        prevEl: '.main__hot-offer-swiper--prev',
	      },
	      	breakpoints: {
	      	1200:{
	      		spaceBetween: 30
	      	},
	      	767:{
	      		spaceBetween: 20
	      	},
            320: {
                spaceBetween: 10
            },
          }
		});
		var swiper3 = new Swiper('.main__social--slider', {
		  slidesPerView: 'auto',
	      slidesPerGroup: 1,
	      loop: true,
	      loopFillGroupWithBlank: true,
	      	breakpoints: {
	      	1200:{
	      		spaceBetween: 29
	      	},
	      	767:{
	      		spaceBetween: 50
	      	},
            320: {
                spaceBetween: 40
            },
          }
		});
});
